#!/usr/bin/env lua5.1

LUA_PATH="lua/?.lua;?.lua;?"
dofile('RPi_gpio.lua')


local sleep = function(seconds)
  os.execute("sleep "..tostring(seconds).."s")
end

--RPi_gpio.set(5,true) -- piezo electric buzzer
--RPi_gpio.set(5,false) -- piezo electric buzzer

--RPi_gpio.set(17,true) -- un led
--RPi_gpio.set(17,false) -- un led


for i = 1,10 do
   RPi_gpio.set(17,true)
  -- print('Estatus es '..RPi_gpio.get_estatus(17))
   sleep(1)
   RPi_gpio.set(17,false)
  -- print('Estatus es '..RPi_gpio.get_estatus(17))
   sleep(1)
end
RPi_gpio.clean(17) -- cierro este pinout


for i = 1,1000 do
   RPi_gpio.set(5,true)
   sleep(0.1)
   RPi_gpio.set(5,false)
--   sleep(0.3)
end
RPi_gpio.clean(5) -- cierro este pinout

--RPi_gpio.toggle(17) -- invierto el estado del pinout 17
--RPi_gpio.clean(17) -- limpio/borro de la tabla del kernel este pinout

