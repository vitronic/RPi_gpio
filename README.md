RPi_gpio
=======
# Inicio Rapido
Para inciar incluye la clase en tu proyecto
dofile('RPi_gpio.lua')

luego ya puedes usar los metodos disponibles hasta ahora:

* RPi_gpio.date() --[[ funcion que retorna el datetime formateado ]]--
* trim(s) --[[ Limpia los espacios al inicio y final de una cadena ]]--
* RPi_gpio.get(number) --[[ Retorna el valor de un pin-in ]]--
* RPi_gpio.set(number,value) --[[ Cambia el estado de un pin-out ]]--
* RPi_gpio.clean(number) --[[ Borra del kernel el gpio ]]--
* RPi_gpio.toggle(number) --[[ Invierte el estado de un pin-out ]]--

Por favor revisa el esqueto archivo pruebas.lua para hacerte una idea de 
la implementacion.
