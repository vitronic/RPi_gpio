--[[--
  RPi_gpio

  Lua Version => 5.1
  @descripcion clase para manipulacion de GPIO en raspberry
  @category GPIO
  @package  RPi_gpio
  @author   Diaz Devera Victor (Master Vitronic) <vitronic2@gmail.com>
  @license  GPLv2  {@link https://www.gnu.org/licenses/gpl-2.0.html}
  @documentacion https://www.kernel.org/doc/Documentation/gpio/sysfs.txt
  @todo implementar si es posible, PWM 
--]]--

RPi_gpio = {} -- Declaro esto como una tabla (Array)
RPi_gpio.__index = RPi_gpio -- Este sera el indice de esta clase


--[[ funcion que retorna el datetime formateado ]]--
function RPi_gpio.date()

  return os.date(" %d/%m/%Y %H:%M:%S")

end

--[[ Limpia los espacios al inicio y final de una cadena ]]--
function trim(s)

  return (s:gsub("^%s*(.-)%s*$", "%1"))

end

--[[ Inicializa el puerto gpio ]]--
local function setup(number)

    local setup = io.open("/sys/class/gpio/export", "a")
    setup:write(number)
    setup:close()

end

--[[ Retorna el valor de la direccion del pin (in o out) ]]--
local function get_direction(number)

    local value = io.open("/sys/class/gpio/gpio"..number.."/direction", "r")
    local result = value:read()
    value:close()
    return trim(result)

end

--[[ Setea la direccion de un pin (in o out )]]--
local function set_direction(number,direction)

    local mode = io.open("/sys/class/gpio/gpio"..number.."/direction", "a")
    mode:write(direction)
    mode:close()

end

--[[ Retorna el valor de un pin gpio ]]--
local function get_estatus(number)

      local value = io.open("/sys/class/gpio/gpio"..number.."/value", "r")
      local result = value:read()
      value:close()
      return result

end

--[[ Retorna el valor de un pin-in ]]--
function RPi_gpio.get(number)

    if get_direction(number) == 'in' then
      return trim(get_estatus(number))
    else
      return false
    end

end

--[[ Cambia el estado de un pin-out ]]--
function RPi_gpio.set(number,value)

    local boolean = (value == true and 1 or 0) --chequeo de sanidad (solo acepto valor boleano)
    setup(number)
    set_direction(number,'out')
    local action = io.open("/sys/class/gpio/gpio"..number.."/value", "a")
    action:write(boolean)
    action:close()

end

--[[ Borra del kernel el gpio ]]--
function RPi_gpio.clean(number)

    local clean = io.open("/sys/class/gpio/unexport", "a")
    clean:write(number)
    clean:close()

end

--[[ Invierte el estado de un pin-out ]]--
function RPi_gpio.toggle(number)

    if get_direction(number) == 'out' then
      RPi_gpio.set(number,( get_estatus(number) == '1' and '0' or true ))
    else
      return false
    end    

end
